from marshmallow import Schema, fields


class UserSchema(Schema):
    name = fields.String()
    last_name = fields.String()
    email = fields.String()
    age = fields.Integer()
