from rest_framework.response import Response
from rest_framework.views import APIView

from app.users.models import User
from app.users.schemas import UserSchema


class ListUsers(APIView):
    def get(self, request):
        data = User.objects.all()
        serialized = UserSchema(many=True).dump(data)
        return Response(serialized)

