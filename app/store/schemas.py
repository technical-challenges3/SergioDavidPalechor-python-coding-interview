from marshmallow import Schema, fields


class ProductSchema(Schema):
    name = fields.String(required=True)
