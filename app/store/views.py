from rest_framework.response import Response
from rest_framework.views import APIView

from app.store.models import Products
from app.store.schemas import ProductSchema


class ListProducts(APIView):
    def post(self, request):
        body = ProductSchema().load(request.data)
        product = Products(**body)
        product.save()
        return Response(ProductSchema().dump(product))
